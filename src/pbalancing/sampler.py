# The MIT License (MIT)
#
# Copyright (c) 2018 Timo Lubitz
# Copyright (c) 2022 Elad Noor, Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import time
from typing import Optional, Tuple

import cvxpy as cp
import hopsy
import numpy as np
import pandas as pd
from path import Path

from . import logger
from .balancer import ParameterBalancing


class PosteriorSampler(object):
    """Handles posterior sampling."""

    def __init__(
        self,
        path: str,
        pb: Optional[ParameterBalancing] = None,
        step_size: float = 1.0,
        n_samples: int = 2500,
        n_thinning: int = 5,
        n_chains: int = 4,
        proposal_type=hopsy.BilliardMALAProposal,
        # proposal_type = hopsy.CSmMALAProposal,
    ) -> None:
        self.pb = pb
        self.path = Path(path)
        self.step_size = step_size
        self.n_samples = n_samples
        self.n_thinning = n_thinning
        self.n_chains = n_chains
        self.proposal_type = proposal_type

    def _check_and_fix_start_point(
        self,
        mean: np.ndarray,
        cov: np.ndarray,
        inequality_matrix: np.ndarray,
        inequality_rhs: np.ndarray,
        epsilon: float = 1e-3,
    ) -> np.ndarray:
        """
        For cases where the mean violates the constraints, the starting point of the polytope should be
        set to a point which is close to the mean in euklid distance, but which does not violate the constraints.
        epsilon controlls how close to the constraints the fixed point can be.

        epsilon: should always be positive
        """

        slacks = np.min(inequality_rhs - inequality_matrix @ mean)
        if slacks <= epsilon:
            # we have to fix mean using qp
            fixed_mean = cp.Variable(mean.shape[0])
            # use the covariance as metric for distance
            R = np.linalg.cholesky(cov)
            objective = cp.Minimize(cp.sum_squares(R @ fixed_mean - mean))

            epsilon_vector = epsilon * np.ones(inequality_rhs.shape[0])
            constraints = [
                inequality_matrix @ fixed_mean
                <= inequality_rhs.flatten() - epsilon_vector
            ]
            prob = cp.Problem(objective, constraints)
            result = prob.solve()
            fixed_slacks = np.min(
                inequality_rhs - inequality_matrix.dot(fixed_mean.value)
            )
            if fixed_slacks * 10 <= epsilon:
                logger.error(f"fixed_slacks = {fixed_slacks}")
                raise RuntimeError("could not fix mean with quadratic programming")
            return fixed_mean.value
        else:
            return mean

    def sample(
        self,
        lower_bounds: float = -1000.0,
        upper_bounds: float = 1000.0,
    ) -> Tuple[np.ndarray, np.ndarray, float, float]:

        mean = pd.read_csv(self.path / "MeanVector.csv", index_col=0).values.flatten()
        cov = pd.read_csv(self.path / "CovarianceMatrix.csv", index_col=0).values
        inequality_matrix = pd.read_csv(
            self.path / "InequalityConstraintMatrix.csv", index_col=0
        ).values
        inequality_rhs = pd.read_csv(
            self.path / "InequalityConstraintRighthandSide.csv", index_col=0
        ).values.flatten()

        model = hopsy.Gaussian(mean, cov)
        problem = hopsy.add_box_constraints(
            hopsy.Problem(inequality_matrix, inequality_rhs, model),
            lower_bounds,
            upper_bounds,
        )

        mean = self._check_and_fix_start_point(
            mean, cov, inequality_matrix, inequality_rhs
        )
        markov_chains = [
            hopsy.MarkovChain(problem, self.proposal_type, starting_point=mean)
            for i in range(self.n_chains)
        ]

        for mc in markov_chains:
            mc.proposal.stepsize = self.step_size

        # one random number generator is required for every chain. technically you should make sure, that all rngs have different seeds.
        rng = [
            hopsy.RandomNumberGenerator(np.random.randint(1, 250000))
            for i in range(self.n_chains)
        ]

        start_time = time.time()
        acceptance_rates, samples = hopsy.sample(
            markov_chains,
            rng,
            self.n_samples,
            thinning=self.n_thinning,
            n_threads=self.n_chains,
        )
        runtime = float(time.time() - start_time)

        logger.debug(f"runtime: {runtime:.1f} seconds")

        # rhat < 1.1 indicates convergence of samples
        rhat = np.max(hopsy.rhat(samples))
        logger.debug(f"rhat: {rhat:.1f}")

        # acceptance rate close to 0 or 1 are problematic for efficiency
        logger.debug(f"acceptance rate: {acceptance_rates}")

        return acceptance_rates, samples, rhat, runtime
