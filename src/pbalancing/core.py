# The MIT License (MIT)
#
# Copyright (c) 2018 Timo Lubitz
# Copyright (c) 2022 Elad Noor, Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import copy
import os

import libsbml
import pkg_resources
from path import Path
from sbtab import validatorSBtab
from sbtab.SBtab import SBtabError

from . import balancer, kineticizer, logger
from .balancer import ParameterBalancingError
from .util import (
    DEFAULT_PARAMETERS,
    TRANSFER_MODE_DICT,
    extract_pseudos_priors,
    id_checker,
    open_sbtabdoc,
    readout_config,
    valid_prior,
)


def parameter_balancing_wrapper(
    sbml_path: Path,
    sbtab_path: Path = None,
    no_pseudo_values: bool = False,
):
    """
    wrapper for parameter balancing.

    Parameters
    ==========
    sbml: string (path to sbml input file)
    sbtab_path: string (path to sbtab input file)
    output_path: string (a path to write the output files)
    no_pseudo_values: Boolean (disable usage of pseudo values)
    """
    model_name = sbml_path
    parameter_dict = {}

    ###########################
    # read the SBML model from the provided input file
    reader = libsbml.SBMLReader()
    if not os.path.exists(sbml_path):
        raise ParameterBalancingError(f"The SBML file {sbml_path} could not be found")

    try:
        sbml_file = reader.readSBML(sbml_path)
        sbml_model = sbml_file.getModel()
    except Exception:
        raise ParameterBalancingError(f"The SBML file {sbml_path} is corrupt")

    if sbml_model.getNumReactions() > 250:
        raise ParameterBalancingError(
            "The given model has more than 250 reactions and we "
            "do not recommend employing models that large for "
            "parameter balancing."
        )
    pb = balancer.ParameterBalancing(sbml_model)

    ###########################
    # read the input SBtab file
    if sbtab_path:
        if not os.path.exists(sbtab_path):
            logger.error(f"Cannot find the SBtab data file: '{sbtab_path}'")
        try:
            sbtabdoc = open_sbtabdoc(sbtab_path)
        except SBtabError as e:
            logger.error(f"The input SBtab is not valid: '{sbtab_path}'")
            raise e
        sbtab_data = sbtabdoc.get_sbtab_by_id("ParameterData")
        sbtab_prior = sbtabdoc.get_sbtab_by_id("Prior")
        sbtab_options = sbtabdoc.get_sbtab_by_id("ConfigurePB")
    else:
        sbtab_data = None
        sbtab_prior = None
        sbtab_options = None

    # open and prepare the paramter data SBtabTable
    if sbtab_data is not None:
        for warning in validatorSBtab.ValidateTable(sbtab_data).return_output():
            logger.warning(warning)

    # prepare a prior SBtabTable (either from the input file, or default)
    if sbtab_prior is None:
        sbtab_prior = open_sbtabdoc(
            pkg_resources.resource_filename("pbalancing", "data/pb_prior.tsv")
        ).get_sbtab_by_id("Prior")

    pb.get_parameter_information(sbtab_prior)
    for warning in validatorSBtab.ValidateTable(sbtab_prior).return_output():
        logger.warning("SBtab prior file: " + warning)
    for warning in valid_prior(sbtab_prior):
        logger.warning("SBtab prior file: " + warning)
    pseudos, priors, pmin, pmax = extract_pseudos_priors(sbtab_prior)

    # prepare a configuration SBtabTable (either from the input file, or default)
    if sbtab_options is None:
        sbtab_options = open_sbtabdoc(
            pkg_resources.resource_filename("pbalancing", "data/pb_options.tsv")
        ).get_sbtab_by_id("ConfigurePB")

    sbtab_options_validate = validatorSBtab.ValidateTable(sbtab_options)

    for warning in sbtab_options_validate.return_output():
        logger.warning("SBtab options file: " + warning)
    parameter_dict, log = readout_config(sbtab_options)
    for warning in log:
        logger.warning("SBtab options file: " + warning)

    # Make empty SBtab if required
    if sbtab_path:
        sbtab = pb.make_sbtab(
            sbtab_data, sbtab_path, "All organisms", pmin, pmax, parameter_dict
        )
        for warning in id_checker(sbtab, sbml_model):
            logger.warning("SBtab data file: " + warning)
    else:
        sbtab = pb.make_empty_sbtab(pmin, pmax, parameter_dict)

    # use default values for all keys that are missing in `parameter_dict`
    _parameter_dict = DEFAULT_PARAMETERS.copy()
    _parameter_dict.update(parameter_dict)
    parameter_dict = _parameter_dict

    ##
    logger.info("Balancing Parameters")

    if parameter_dict["use_pseudo_values"] == "True" and not no_pseudo_values:
        sbtab_old = copy.deepcopy(sbtab)
        sbtab_new = pb.fill_sbtab(sbtab_old, pseudos, priors)
        logger.info("Parameter balancing is using pseudo values.")

    else:
        sbtab_new = pb.fill_sbtab(sbtab)
        logger.info("Parameter balancing is not using pseudo values.")

    (
        sbtab_final,
        mean_vector,
        mean_vector_inc,
        c_post,
        c_post_inc,
        r_matrix,
        shannon,
        concat_file,
    ) = pb.make_balancing(sbtab_new, sbtab, pmin, pmax, parameter_dict)

    ##
    logger.info("Inserting parameters and kinetics into SBML model")
    clear_mode = parameter_dict.get("parametrisation", "equilibrium constant")
    mode = TRANSFER_MODE_DICT.get(clear_mode, "hal")
    enzyme_prefac = parameter_dict.get("prefac", True)
    def_inh = parameter_dict.get("default_inh", "complete_inh")
    def_act = parameter_dict.get("default_act", "complete_act")
    kineticizer.KineticizerCS(
        sbml_model, sbtab_final, mode, enzyme_prefac, def_inh, def_act, True
    )

    return sbml_model, sbtab_final
