# The MIT License (MIT)
#
# Copyright (c) 2018 Timo Lubitz
# Copyright (c) 2022 Elad Noor, Weizmann Institute of Science
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import argparse
import logging
import os
import sys
from zipfile import ZipFile

from path import Path

from pbalancing import logger
from pbalancing.core import parameter_balancing_wrapper


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("sbml", help="Path to SBML input file")
    parser.add_argument("--sbtab", help="Path to SBtab input file")
    parser.add_argument("--output_path", help="Path for writing the output file")
    parser.add_argument(
        "-d", "--debug", action="store_true", help="full application debug mode"
    )
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="suppress all console output"
    )
    parser.add_argument(
        "-p",
        "--no_pseudo_values",
        help="Flag for disabling the usage of pseudo values.",
        action="store_true",
    )

    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
    elif args.quiet:
        logger.setLevel(logging.ERROR)
    else:
        logger.setLevel(logging.WARNING)

    sbml_path = Path(args.sbml)

    if not args.sbtab:
        sbtab_path = None
    else:
        sbtab_path = Path(args.sbtab)

    sbml_model, sbtab_final = parameter_balancing_wrapper(
        sbml_path=sbml_path,
        sbtab_path=sbtab_path,
        no_pseudo_values=args.no_pseudo_values,
    )

    if args.output_path:
        output_path = Path(args.output_path)
    else:
        output_path = sbml_path.parent / sbml_path.stem

    if output_path.ext != ".zip":
        output_path = output_path + ".zip"

    if os.path.exists(output_path):
        if (
            input(
                f"Are you sure you want to overwrite the output file ({output_path})? [y/n] "
            ).lower()
            != "y"
        ):
            sys.exit(0)

    with ZipFile(output_path, "w") as zip:
        zip.writestr(
            sbml_path.stem + ".xml",
            '<?xml version="1.0" encoding="UTF-8"?>\n' + sbml_model.toSBML(),
        )
        zip.writestr(sbml_path.stem + ".tsv", sbtab_final.to_str())
