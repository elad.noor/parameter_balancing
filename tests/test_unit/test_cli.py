"""unit test for parameter balancing CLI."""
# The MIT License (MIT)
#
# Copyright (c) 2022 Weizmann Institute of Science
# Copyright (c) 2022 INRAE
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest
from sbtab.SBtab import SBtabError, read_csv

from pbalancing import logger, parameter_balancing_wrapper


@pytest.mark.parametrize(
    "example_name", [("ecoli_carlsson_modified"), ("teusink"), ("hynne")]
)
def test_cli(example_name, test_dir):
    """Test the the CLI."""
    sbml_path = test_dir / f"{example_name}_model.xml"
    sbtab_path = test_dir / f"{example_name}.tsv"

    assert sbml_path.exists()
    assert sbtab_path.exists()

    sbml_model_new, sbtab_final = parameter_balancing_wrapper(
        sbml_path=sbml_path,
        sbtab_path=sbtab_path,
        no_pseudo_values=False,
    )
    df_final = sbtab_final.to_data_frame()

    try:
        sbtab_ref = read_csv(
            test_dir / f"reference_{example_name}.tsv", "reference.tsv"
        ).get_sbtab_by_id("ParameterBalanced")
    except SBtabError as e:
        logger.error(f"The reference SBtab is not valid: '{sbtab_path}'")
        raise e
    df_ref = sbtab_ref.to_data_frame()

    for col in ["Mode", "UnconstrainedStd", "UnconstrainedMean"]:
        v_final = df_final[col].apply(float).values
        v_ref = df_ref[col].apply(float).values
        assert v_final == pytest.approx(v_ref, rel=1e-3)
