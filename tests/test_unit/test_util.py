"""unit test for basic utility functions."""
# The MIT License (MIT)
#
# Copyright (c) 2022 Weizmann Institute of Science
# Copyright (c) 2022 INRAE
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import numpy as np
import pytest
from numpy.linalg import inv

from pbalancing.util import fmin_cvxpy, fmin_gen


def test_fmin_gen():
    """Unit-test the fmin_gen function."""
    median = np.array([1.0, 1.0, 1.0, -1.0, -1.0])
    N = median.shape[0]
    cov = np.eye(N)
    bounds = [(0.0, 0.5)] * N
    variable_is_logarithmic = [False] * N

    solution = np.array([0.5, 0.5, 0.5, 0.0, 0.0])

    cvxpy_solution = fmin_cvxpy(
        median=median,
        cov=cov,
        bounds=bounds,
        variable_is_logarithmic=variable_is_logarithmic,
    )
    assert cvxpy_solution == pytest.approx(solution, abs=1e-3)

    np.random.seed(2019)
    x0 = np.random.rand(N)
    f_quad = lambda q: (q - median).T @ inv(cov) @ (q - median)
    genetic_solution = fmin_gen(
        f_opt=f_quad,
        x0=x0,
        medians=median,
        C_post=cov,
        population_size=100,
        survivors=20,
        generations=1000,
        bounds=bounds,
        variable_is_logarithmic=variable_is_logarithmic,
        intruders=0,
    )

    # after 1000 generations, the solution should converge to the medians
    # regardless of the starting point
    assert genetic_solution == pytest.approx(solution, abs=1e-3)
